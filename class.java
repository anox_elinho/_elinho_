package susu;

import javafx.util.Pair;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

class Edge {
    final int from;
    final int to;
    int weight;

    public Edge(int from, int to, int weight) {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public Edge(Edge other) {
        from = other.from;
        to = other.to;
        weight = other.weight;
    }

    public boolean isBack() {
        return from>to;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Edge) {
            Edge e = (Edge)obj;
            return e.from==from && e.to==to && e.weight==weight;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return from*397+to*13+weight*17;

    }
}

interface IGraph {
    void readGraph(String fileName);
    void writeGraph(String fileName);

    void addEdge(int from,int to, int weight);
    void removeEdge(int from,int to);
    int changeEdge(int from,int to,int newWeight);

    void transformToAdjList();
    void transformToAdjMatrix();
    void transformToListOfEdges();

    boolean checkBipart();
    List<Pair<Integer,Integer>> getMaximumMatchingBipart();

    int checkEuler();
    ArrayList<Integer> getEuleranTourFleri();
    ArrayList<Integer> getEuleranTourEffective();

    Graph getSpaingTreePrima();
    Graph getSpaingTreeKruscal();
    Graph getSpaingTreeBoruvka();

    Graph ?owFordFulkerson(int sourse, int sink);
    Graph ?owDinitz(int sourse, int sink);

}

enum GraphType {
    EdgesList,
    AdjectionList,
    AdjectionMatrix
}

class DSU {

    private final int[] p;
    private final int[] rank;

    DSU(int n) {
        p = new int[n];
        rank = new int[n];
        for (int i = 0; i < n; i++) {
            p[i] = i;
        }
    }

    public void makeSet(int x) {
        p[x] = x;
    }

    int find(int x) {
        if (p[x]==x)
            return x;

        return p[x] = find(p[x]);
    }

    void unite(int x, int y) {
        x = find(x);
        y = find(y);

        if (rank[x]<rank[y]) {
            p[x] = y;
        }
        else {
            p[y] = x;
            if (rank[x]==rank[y])
                ++rank[x];
        }
    }
}

class Graph implements IGraph {

    List<Edge> edgesList = new ArrayList<>();
    int[][] adjMatrix = new int[0][0];
    ArrayList<HashMap<Integer, Integer>> adjList = new ArrayList<>();
    boolean isWeighted = false;
    boolean isDirected = false;
    GraphType graphType;

    private int vertexCount = 0;
    private int edgesCount = 0;

    int vertexCount() {
        return vertexCount;
    }
    int edgesCount() {
        return isDirected ? edgesCount : edgesCount / 2;
    }

    private Graph(int n) {
        vertexCount = n;
    }

    public Graph() { }

    public Graph(String filePath) { readGraph(filePath);}

    //region ������������
    private int[] marks;
    private int[] matching;
    private boolean[] visited;

    public boolean checkBipart() {

        if (vertexCount()<2)
            return false;

        Stack<Integer> stack = new Stack<>();
        marks = new int[vertexCount()];

        Arrays.fill(marks, -1);
        transformToAdjList();
        stack.push(0);
        marks[0] = 1;

        while (!stack.empty()) {
            Integer vert = stack.pop();
            int m = marks[vert]==1 ? 2 : 1;

            Set<Integer> neig = adjList.get(vert).keySet();
            for (int n : neig) {
                if (marks[n]==-1) {
                    marks[n] = m;
                    stack.push(n);
                }
                else if (marks[n]!=m)
                    return false;
            }
        }
        return true;
    }


    public List<Pair<Integer,Integer>> getMaximumMatchingBipart() {
        if (!checkBipart()) return null;

        matching = new int[vertexCount()];
        Arrays.fill(matching, -1);
        visited = new boolean[vertexCount()];
        Arrays.fill(visited, false);

        for (int i = 0; i < vertexCount(); i++) {
            if (marks[i]==1) {
                khunStep(i);
            }
        }

        List<Pair<Integer,Integer>> res = new ArrayList<>();
        for (int i = 0; i < vertexCount(); i++) {
            if (matching[i]!=-1 && matching[i]>i) {
                res.add(new Pair<>(i+1,matching[i]+1));
            }
        }
        return res;
    }

    private boolean khunStep(int vert) {
        if (visited[vert]) return false;

        visited[vert] = true;
        Set<Integer> neig = adjList.get(vert).keySet();
        for (int v : neig) {
            int m = matching[v];
            if (m==-1) {
                matching[v] = vert;
                matching[vert] = v;
                return true;
            }
        }
        for (int v: neig) {
            int m = matching[v];
            if (khunStep(m)) {
                matching[vert] = v;
                matching[v] = vert;
                return true;
            }
        }
        return false;
    }

    //endregion

    //region �����

    /**
     * �������� ������������� �������� �����
     * @return ���� ���������� ����, ������ ����� �������, �� ������� ����� ������ �����.
     * ���� ���������� ���� - ������������� ����� ��������� �������.
     * ���� ��� �� ����, �� ����� - 0.
     */
    public int checkEuler() {
        transformToAdjList();
        DSU dsu = new DSU(vertexCount());
        int odd = 0;
        int index = 0;
        for (int i = 0; i < adjList.size(); i++) {
            if (adjList.get(i).size()%2!=0) {
                odd++;
                index = i;
            }
            for (Map.Entry<Integer, Integer> near : adjList.get(i).entrySet()) {
                dsu.unite(i,near.getKey());
            }
        }

        for (int i = 1; i < vertexCount(); i++) {
            if (dsu.find(i)!=dsu.find(0) && adjList.get(i).size()!=0) { //� ����� ������������ ��������� ��������� ��������� � �������
                return 0;
            }
        }

        if (odd>2)
            return 0;
        else if (odd==0)      //���� ����, ���������� ������������� ��������
            return index+1;
        else return -(index+1); //���� ����, ������������� ��������
    }


    public ArrayList<Integer> getEuleranTourFleri() {
        Graph graph = deepCopyGraph();
        ArrayList<Integer> resList = new ArrayList<>(vertexCount());

        int start = Math.abs(graph.checkEuler())-1;
        if (start==-1) return resList;//��� ����

        graph.transformToAdjList();

        Edge next = new Edge(0,start,0); //�������� � ���������� ��� �������� �������
        resList.add(start+1);

        while (graph.edgesCount()!=0) {
            List<Edge> near = graph.NearEdges(next.to);

            Optional<Edge> notBridge = near.stream().filter(e -> !graph.isBridge(e)).findFirst();
            next = notBridge.isPresent() ? notBridge.get() : near.get(0);
            graph.removeEdge(next.from,next.to);

            resList.add(next.to+1);
        }

        return resList;
    }

    public ArrayList<Integer> getEuleranTourEffective() {
        Graph graph = deepCopyGraph();
        ArrayList<Integer> resList = new ArrayList<>(vertexCount());
        Stack<Integer> stack = new Stack<>();

        int start = Math.abs(graph.checkEuler())-1;
        if (start==-1) return resList;//��� ����

        graph.transformToAdjList();
        Edge next = new Edge(0,start,0);
        stack.push(start);

        while (graph.edgesCount()!=0) {
            List<Edge> nearEdges = graph.NearEdges(next.to);
            if (nearEdges.size()!=0) {
                next = nearEdges.get(0);
                graph.removeEdge(next.from,next.to);
            }
            else { //������ � �����
                resList.add(stack.pop()+1);
                next = new Edge(0,stack.pop(),0);//������������ �� 1 ������� �����
            }
            stack.add(next.to);
        }

        while (!stack.isEmpty()) {
            resList.add(stack.pop()+1);
        }

        return resList;
    }

    private boolean BFS(int from, int to) {
        transformToAdjList();
        Stack<Integer> stack = new Stack<>();
        boolean[] used = new boolean[vertexCount()];
        stack.add(from);

        while (!stack.isEmpty()) {
            int i = stack.pop();
            if (i==to)
                return false;

            used[i] = true;
            Set<Integer> neig = adjList.get(i).keySet();

            for (Integer v : neig) {
                if (!used[v] && !stack.contains(v)) stack.push(v);
            }
        }
        return true;
    }

    private boolean isBridge(Edge edge) {
        removeEdge(edge.from,edge.to);
        boolean res = BFS(edge.from, edge.to);
        addEdge(edge.from, edge.to, edge.weight);
        return res;
    }

    //endregion

    //region MST


    /*public Graph getSpaingTreePrima() {
        Graph graph = copyGraph();
        transformToListOfEdges();
        HashSet<Integer> selVertex = new HashSet<>();
        selVertex.add(0);//�������� ��������� �������

        while (selVertex.size() != vertexCount()) {
            Edge minEdge = null;
            for(Edge e : edgesList) {
                if (!selVertex.contains(e.from) || selVertex.contains(e.to)) continue;
                if (minEdge==null || minEdge.weight > e.weight)
                    minEdge = e;
            }
            graph.addEdge(minEdge.from,minEdge.to,minEdge.weight);
            selVertex.add(minEdge.to);

            Edge edge = edgesList.stream()
                    .filter(e -> selVertex.contains(e.from) && !selVertex.contains(e.to)) //�������� �����, ���� ����� ������� ����� � ��������� ��������, � ������ - ���
                    .min((a, b) -> a.weight - b.weight) // ����� ��� �������� �����������
                    .get();
            //graph.addEdge(edge.from, edge.to, edge.weight);//��������� ��� � �����
            //selVertex.add(edge.to);
        }
        return graph;
    }*/

    public Graph getSpaingTreePrima() {
        Graph graph = copyGraph();
        transformToAdjList();

        boolean[] used = new boolean[vertexCount()];
        int[] key = new int[vertexCount()];
        int[] parent = new int[vertexCount()];
        Arrays.fill(key,-1);
        Arrays.fill(parent,-1);

        Comparator<Pair<Integer,Integer>> comparator = (a, b) -> {
            if (a.getValue()== -1 && b.getValue()!=-1) return 1;
            if (a.getValue() != -1 && b.getValue()==-1) return -1;
            if (a.getValue()>b.getValue()) return 1;
            if (a.getValue()==b.getValue()) return a.getValue()-b.getValue();
            return -1;
        };
        PriorityQueue<Pair<Integer,Integer>> queue = new PriorityQueue<>(comparator);

        key[0] = 0;
        for (int i = 0; i < vertexCount(); i++) {
            queue.add(new Pair<>(i,key[i]));
        }

        while (!queue.isEmpty()) {
            int min = queue.peek().getKey();
            key[min] = queue.peek().getValue();
            queue.remove();

            if (used[min]) continue;

            used[min] = true;
            if (parent[min] != -1) {
                graph.addEdge(min,parent[min], adjList.get(min).get(parent[min]));
            }

            for (Map.Entry<Integer, Integer> edge : adjList.get(min).entrySet()) {
                int from = edge.getKey();
                int to = edge.getValue();
                if (!used[from] && ((to < key[from]) || (key[from] == -1))) {
                    key[from] = to;
                    parent[from] = min;
                    queue.add(new Pair<>(from,key[from]));
                }
            }
        }
        return graph;

    }

    public Graph getSpaingTreeKruscal() {
        Graph graph = copyGraph();

        DSU dsu = new DSU(vertexCount);
        transformToListOfEdges();

        edgesList.sort((a, b) -> a.weight - b.weight);

        int remainingEdges = vertexCount - 1;
        int index = 0;
        while (remainingEdges > 0) {
            Edge e = edgesList.get(index);

            if (e.isBack()) {
                index++;
                continue; //���������� �������� �����
            }

            if (dsu.find(e.from) != dsu.find(e.to)) //������� ����� � ������ ����������� ���������
            {
                graph.addEdge(e.from, e.to, e.weight);
                remainingEdges--;
                dsu.unite(e.from, e.to);
            }
            index++;
        }

        return graph;
    }

    public Graph getSpaingTreeBoruvka() {
        Graph mst = copyGraph();

        DSU dsu = new DSU(vertexCount);
        transformToListOfEdges();

        for (int t = 1; t < this.vertexCount() && mst.edgesCount() < this.vertexCount() - 1; t = t + t) {

            Edge[] closest = new Edge[vertexCount()];
            for (Edge e : edgesList) {
                int i = dsu.find(e.from), j = dsu.find(e.to);
                if (i == j) continue;   // ���� ������
                if (closest[i] == null || e.weight<closest[i].weight) closest[i] = e;
                if (closest[j] == null || e.weight<closest[j].weight) closest[j] = e;
            }

            // ��������� � mst
            for (int i = 0; i < vertexCount(); i++) {
                Edge e = closest[i];
                if (e != null) {
                    if (dsu.find(e.from)!=dsu.find(e.to)) {
                        mst.addEdge(e.from,e.to,e.weight);
                        dsu.unite(e.from,e.to);
                    }
                }
            }

        }
        return mst;
    }
//endregion

    //region ������ �����
    @Override
    public void readGraph(String fileName) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(fileName));
            String input = String.join(System.lineSeparator(), lines);

            Scanner scanner = new Scanner(input);
            char inputType = scanner.next().charAt(0);
            vertexCount = scanner.nextInt();
            scanner.nextLine();
            isDirected = scanner.nextInt() == 1;
            isWeighted = scanner.nextInt() == 1;
            scanner.nextLine();

            switch (inputType) {
                case 'C':
                    readAdjMatrix(scanner);
                    break;
                case 'L':
                    readAdjList(scanner);
                    break;
                case 'E':
                    readEdgesList(scanner);
                    break;
                default: throw new IllegalArgumentException("��� ������� ������ �� ���������!");
            }
            scanner.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void readAdjMatrix(Scanner scanner) {

        adjMatrix = new int[vertexCount][vertexCount];

        for (int i = 0; i < vertexCount; i++) {
            for (int j = 0; j < vertexCount; j++) {
                adjMatrix[i][j] = scanner.nextInt();
                if (adjMatrix[i][j]!=0)
                    edgesCount++;
            }
        }

        graphType = GraphType.AdjectionMatrix;
    }

    private void readEdgesList(Scanner scanner) {

        while (scanner.hasNextLine()) {
            int[] nums = getNums(scanner);

            /*
            nums[0] - from
            nums[1] - to
            nums[2] - weight
             */
            edgesList.add(new Edge(nums[0]-1, nums[1]-1, isWeighted ? nums[2] : 0));
            if (!isDirected) {
                edgesList.add(new Edge(nums[1]-1,nums[0]-1,isWeighted ? nums[2] : 0));//��������� �������� �����
            }
        }
        edgesCount = edgesList.size();
        graphType = GraphType.EdgesList;
    }

    private void readAdjList(Scanner scanner) {

        adjList = new ArrayList<>(vertexCount);

        for (int i = 0; i < vertexCount; i++) {
            int[] nums = getNums(scanner);
            adjList.add(new HashMap<>());

            if (isWeighted) {
                for (int j = 0; j < nums.length / 2; j++) {
                    adjList.get(i).put(nums[2*j]-1,nums[2*j+1]);
                    edgesCount++;
                }
            }
            else {
                for (int num : nums) {
                    adjList.get(i).put(num - 1, 0);
                }
                edgesCount += nums.length;
            }
        }

        graphType = GraphType.AdjectionList;
    }

    private int[] getNums(Scanner scanner) {
        String[] line = scanner.nextLine().split(" ");
        return Arrays.stream(line)
                .mapToInt(Integer::parseInt)
                .toArray();
    }



//endregion

    //region �������� ��� �������
    @Override
    public void addEdge(int from, int to, int weight) {

        switch (graphType) {
            case EdgesList:
                edgesList.add(new Edge(from, to, weight));

                if (!isDirected) {
                    edgesList.add(new Edge(to, from, weight));
                }
                break;

            case AdjectionList:
                adjList.get(from).put(to, weight);

                if (!isDirected) {
                    adjList.get(to).put(from, weight);
                }
                break;

            case AdjectionMatrix:
                adjMatrix[from][to] = weight;

                if (!isDirected) {
                    adjMatrix[to][from] = weight;
                }
                break;
        }
        if (isDirected)
            edgesCount++;
        else edgesCount += 2;
    }

    @Override
    public void removeEdge(int from, int to) {

        switch (graphType) {

            case EdgesList:

                edgesList.remove(findEdge(from, to));

                if (!isDirected) {
                    edgesList.remove(findEdge(to, from));
                }
                break;

            case AdjectionList:
                adjList.get(from).remove(to);

                if (!isDirected) {
                    adjList.get(to).remove(from);
                }
                break;

            case AdjectionMatrix:
                adjMatrix[from][to] = 0;

                if (!isDirected) {
                    adjMatrix[to][from] = 0;
                }
                break;
        }

        if (isDirected)
            edgesCount--;
        else edgesCount -= 2;
    }

    @Override
    public int changeEdge(int from, int to, int newWeight) {

        int oldWeight = 0;

        switch (graphType) {
            case EdgesList:
                Edge edge = findEdge(from, to);
                oldWeight = edge.weight;
                edge.weight = newWeight;

                if (!isDirected) {
                    Edge edgeBack = findEdge(to, from);
                    edgeBack.weight = newWeight;
                }
                break;

            case AdjectionList:
                HashMap<Integer, Integer> map = adjList.get(from);

                oldWeight = map.get(to);
                map.put(to, newWeight);

                if (!isDirected) {
                    adjList.get(to).put(from, newWeight);
                }
                break;


            case AdjectionMatrix:
                oldWeight = adjMatrix[from][to];
                adjMatrix[from][to] = newWeight;

                if (!isDirected) {
                    adjMatrix[to][from] = newWeight;
                }
                break;
        }

        return oldWeight;
    }

//endregion

    //region �������������� �����
    @Override
    public void transformToAdjList() {

        switch (graphType) {
            case EdgesList:
                adjList = FormatConverter.toAdjList(edgesList, vertexCount);
                break;
            case AdjectionMatrix:
                adjList = FormatConverter.toAdjList(adjMatrix);
                break;
        }

        edgesList.clear();
        adjMatrix = null;
        graphType = GraphType.AdjectionList;
    }

    @Override
    public void transformToAdjMatrix() {

        switch (graphType) {
            case EdgesList:
                adjMatrix = FormatConverter.toAdjMatrix(edgesList, vertexCount);
                break;
            case AdjectionList:
                adjMatrix = FormatConverter.toAdjMatrix(adjList);
                break;
        }

        edgesList.clear();
        adjList.clear();
        graphType = GraphType.AdjectionMatrix;
    }

    @Override
    public void transformToListOfEdges() {

        switch (graphType) {
            case AdjectionList:
                edgesList = FormatConverter.toEdgesList(adjList);
                break;
            case AdjectionMatrix:
                edgesList = FormatConverter.toEdgesList(adjMatrix);
                break;
        }
        adjList.clear();
        adjMatrix = null;

        graphType = GraphType.EdgesList;
    }
//endregion

    //region ������ � ����
    @Override
    public void writeGraph(String fileName) {
        BufferedWriter writer = null;
        try {
            File file = new File(fileName);
            writer = new BufferedWriter(new FileWriter(file));

            switch (graphType) {
                case EdgesList:
                    writeEdges(writer);
                    break;
                case AdjectionList:
                    writeList(writer);
                    break;
                case AdjectionMatrix:
                    writeMatrix(writer);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                //noinspection ConstantConditions
                writer.close();
            } catch (Exception ignored) {
            }
        }
    }

    private void writeEdges(BufferedWriter writer) throws IOException {
        writeHeader(writer, 'E');

        for (Edge e : edgesList) {

            if (!isDirected && e.isBack())//���������� �������� �����
                continue;

            String mes = String.format("%1$d %2$d %3$s", e.from + 1, e.to + 1, isWeighted ? e.weight : "").trim();
            writer.write(mes);
            writer.newLine();
        }

    }

    private void writeList(BufferedWriter writer) throws IOException {

        writeHeader(writer, 'L');

        for (HashMap<Integer, Integer> anAdjList : adjList) {
            String line = "";
            for (Map.Entry<Integer, Integer> entry : anAdjList.entrySet()) {
                Integer to = entry.getKey() + 1;
                Integer weight = entry.getValue();

                line = to + " ";
                if (isWeighted)
                    line += weight;
            }
            writer.write(line.trim());
            writer.newLine();
        }
    }

    private void writeMatrix(BufferedWriter writer) throws IOException {

        writeHeader(writer, 'C');

        for (int[] anAdjMatrix : adjMatrix) {
            String line = "";
            for (int j = 0; j < adjMatrix.length; j++) {
                line += anAdjMatrix[j] + " ";
            }
            writer.write(line.trim());
            writer.newLine();
        }
    }

    private void writeHeader(BufferedWriter writer, char outputType) throws IOException {
        if (outputType=='E') {
            writer.write(String.format("%4$s %1$d %5$d\r\n%2$d %3$d\r\n",vertexCount, isDirected ? 1:0, isWeighted? 1:0, outputType, edgesCount()));
        }
        else {
            writer.write(String.format("%4$s %1$d\r\n%2$d %3$d\r\n",vertexCount, isDirected ? 1:0, isWeighted? 1:0, outputType));
        }
    }
//endregion

    //region ������
    private ArrayList<Integer> flowMarks;
    private LinkedList<Map.Entry<Integer, Integer>> dfsWay = new LinkedList<>();
    private Graph workingCopy;
    private int minWeight;

    private boolean isNet(){
        return isDirected && isWeighted;
    }

    private class Checker {
        boolean check(int first, int second) {
            return true;
        }
    }

    private boolean dfs(int source, int sink, ArrayList<Boolean> visited, Checker checker){
        visited.set(source, true);
        if (source == sink) return true;

        for (Map.Entry<Integer, Integer> edge : workingCopy.adjList.get(source).entrySet()) {
            if (visited.get(edge.getKey()))
                continue;
            if (checker.check(source, edge.getKey())){
                if (dfs(edge.getKey(), sink, visited, checker)){
                    dfsWay.push(edge);
                    if (minWeight > edge.getValue()){
                        minWeight = edge.getValue();
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private boolean bfs(int source, int sink){
        LinkedList<Integer> queue = new LinkedList<>();
        for (int i = 0; i< flowMarks.size(); ++i){
            flowMarks.set(i, -1);
        }
        int cursor, layer;
        queue.push(source);
        flowMarks.set(source, 0);
        do{
            cursor = queue.poll();
            layer = flowMarks.get(cursor) + 1;
            Set<Integer> neig = workingCopy.adjList.get(cursor).keySet();

            for (int vertex: neig){
                if (flowMarks.get(vertex) == -1){
                    flowMarks.set(vertex, layer);
                    queue.push(vertex);
                }
            }
        } while (!queue.isEmpty());
        return flowMarks.get(sink) != -1;
    }

    private void feedEdge(int from, int to, int deltaWeight){
        Integer old = adjList.get(from).get(to);
        if (old == null)
            addEdge(from, to, deltaWeight);
        else
            changeEdge(from, to, old + deltaWeight);
    }

    private void dryEdge(int from, int to, int deltaWeight){
        Integer old = adjList.get(from).get(to);
        if (old > deltaWeight)
            changeEdge(from, to, old - deltaWeight);
        else
            removeEdge(from, to);
    }

    public Graph ?owFordFulkerson(int sourse, int sink){
        if (!isNet())
            return null;

        Graph resultFlow = deepCopyGraph();

        workingCopy = deepCopyGraph();
        workingCopy.transformToAdjList();

        boolean found;
        int cursor;
        Checker checker = new Checker();
        Map.Entry<Integer, Integer> temp;

        do {
            found = false;
            cursor = sourse;
            minWeight = Integer.MAX_VALUE;
            ArrayList<Boolean> visited = new ArrayList<>(Collections.nCopies(workingCopy.vertexCount(), false));
            found = dfs(sourse, sink, visited, checker);
            while (!dfsWay.isEmpty()){
                temp = dfsWay.poll();
                System.out.println((cursor+1) + " "+  (temp.getKey()+1) + " " + minWeight);
                workingCopy.dryEdge(cursor, temp.getKey(), minWeight);
                workingCopy.feedEdge(temp.getKey(), cursor, minWeight);
                resultFlow.feedEdge(cursor, temp.getKey(), minWeight);
                cursor = temp.getKey();
            }
        } while (found);
        workingCopy = null;
        return resultFlow;
    }

    public Graph ?owDinitz(int sourse, int sink){
        if (!isNet())
            return null;

        Graph resultFlow = deepCopyGraph();

        workingCopy = deepCopyGraph();
        workingCopy.transformToAdjList();

        flowMarks = new ArrayList<>(Collections.nCopies(workingCopy.vertexCount(), -1));
        int cursor;
        Checker checker = new Checker();
        Map.Entry<Integer, Integer> temp;
        while (bfs(sourse, sink)){
            boolean found;
            do {
                cursor = sourse;
                minWeight = Integer.MAX_VALUE;
                ArrayList<Boolean> visited = new ArrayList<>(Collections.nCopies(workingCopy.vertexCount(), false));
                found = dfs(sourse, sink, visited, checker);
                while (!dfsWay.isEmpty()){
                    temp = dfsWay.poll();
                    workingCopy.dryEdge(cursor, temp.getKey(), minWeight);
                    workingCopy.feedEdge(temp.getKey(), cursor, minWeight);
                    resultFlow.feedEdge(cursor, temp.getKey(), minWeight);
                    cursor = temp.getKey();
                }
            } while (found);
        }
        workingCopy = null;
        flowMarks = null;
        return resultFlow;
    }

    //endregion

//region Utils

    private Graph copyGraph() {
        Graph graph = new Graph(vertexCount);
        graph.graphType = GraphType.EdgesList;
        graph.isDirected = this.isDirected;
        graph.isWeighted = this.isWeighted;
        return graph;
    }

    private Graph deepCopyGraph() {
        Graph graph = copyGraph();
        transformToListOfEdges();
        for(Edge e : edgesList) {
            if (!isDirected && e.from>e.to) continue; //���������� �������� �����
            graph.addEdge(e.from,e.to,e.weight);
        }
        return graph;
    }

    private Edge findEdge(int from, int to) {
        return edgesList.stream().filter(e -> e.from == from && e.to == to).findFirst().get();
    }

    private List<Edge> NearEdges(int vertex) {
        return adjList.get(vertex).entrySet().stream()
                .map(e -> new Edge(vertex,e.getKey(),e.getValue())).collect(Collectors.toList());
    }

//endregion
}

final class FormatConverter {

    //�� ������� ��������� � ������ �����
    public static List<Edge> toEdgesList(int[][] adjMatrix) {

        ArrayList<Edge> result = new ArrayList<>();
        int vertexCount = adjMatrix.length;

        for (int i = 0; i < vertexCount; i++) {
            for (int j = 0; j < vertexCount; j++) {
                int weight = adjMatrix[i][j];
                if (weight!=0) { // ���� �����
                    result.add(new Edge(i,j,weight));
                }
            }
        }
        return result;
    }

    //�� ������ ��������� � ������ �����
    public static List<Edge> toEdgesList(ArrayList<HashMap<Integer,Integer>> adjList) {
        ArrayList<Edge> result = new ArrayList<>();
        for (int i = 0; i < adjList.size(); i++) {
            for (Map.Entry<Integer,Integer> entry : adjList.get(i).entrySet()) {
                result.add(new Edge(i,entry.getKey(),entry.getValue()));
            }
        }
        return result;
    }

    //�� ������ ��������� � ������� ���������
    public static int[][] toAdjMatrix(ArrayList<HashMap<Integer,Integer>> adjList) {
        int vertexCount = adjList.size();
        int[][] result = new int[vertexCount][vertexCount];

        for (int i = 0; i < adjList.size(); i++) {
            for (Map.Entry<Integer,Integer> entry : adjList.get(i).entrySet()) {
                result[i][entry.getKey()] = entry.getValue();
            }
        }

        return result;
    }

    //�� ������ ����� � ������� ���������
    public static int[][] toAdjMatrix(List<Edge> edgesList, int vertexCount) {
        int[][] result = new int[vertexCount][vertexCount];

        for (Edge e : edgesList) {
            result[e.from][e.to] = e.weight;
        }

        return result;
    }

    //�� ������� ��������� � ������ ���������
    public static ArrayList<HashMap<Integer,Integer>> toAdjList(int[][] adjMatrix){

        int vertexCount = adjMatrix.length;
        ArrayList<HashMap<Integer,Integer>> result = new ArrayList<>(vertexCount);
        for (int[] anAdjMatrix : adjMatrix) {
            result.add(new HashMap<>());
        }


        for (int i = 0; i < vertexCount; i++) {
            for (int j = 0; j < vertexCount; j++) {

                if (adjMatrix[i][j]!=0) {
                    result.get(i).put(j,adjMatrix[i][j]);
                }
            }
        }
        return result;
    }

    //�� ������ ����� � ������ ���������
    public static ArrayList<HashMap<Integer,Integer>> toAdjList(List<Edge> edgesList, int vertexCount){
        ArrayList<HashMap<Integer,Integer>> result = new ArrayList<>(vertexCount);
        for (int i = 0; i < vertexCount; i++) {
            result.add(new HashMap<>());
        }

        for (Edge e : edgesList) {
            result.get(e.from).put(e.to,e.weight);
        }

        return result;
    }
}